import React, { createContext, useState } from 'react';
import auth from '@react-native-firebase/auth';
import { Alert } from 'react-native';
import { GoogleSignin } from '@react-native-community/google-signin';

GoogleSignin.configure({
  webClientId: "853964595557-98ksm0jj7dmknq8ephi7ps4oldm7c10v.apps.googleusercontent.com",
});

export const AuthContext = createContext({});

export const AuthProvider = ({ children }) => {
  const [user, setUser] = useState(null);
  const [googleCred, setGoogleCred] = useState(null);
  const [errorMessage, setErrorMessage] = useState('');
  return (
    <AuthContext.Provider
      value={{
        user,
        setUser,
        login: async (email, password) => {
          if (email && password) {
            try {
              await auth().signInWithEmailAndPassword(email, password);
            } catch (e) {
              var errorCode = e.code;
              switch (errorCode) {
                case 'auth/invalid-email':
                  Alert.alert('Invalid email address.');
                  break;
                case 'auth/user-disabled':
                  Alert.alert('This user is disabled.');
                  break;
                case 'auth/user-not-found':
                  Alert.alert('There is no user with this email address.');
                  break;
                case 'auth/wrong-password':
                  Alert.alert('Wrong password passed.');
                  break;
                default:
                  Alert.alert('Unknown error occured.');
              }

            }
          } else {
            Alert.alert('Fill email and password fields.');
          }
        },
        googleLogin: async () => {
          try {
            const { idToken } = await GoogleSignin.signIn().then(console.log('GOOGLE SIGN IN'));
            // Create a Google credential with the token
            const googleCredential = auth.GoogleAuthProvider.credential(idToken);
            setGoogleCred(googleCredential);
            auth().signInWithCredential(googleCredential);
          } catch (e) {
            Alert.alert('Google sign in failed.', e.message);
          }
        },
        register: async (email, password) => {
          if (email && password) {
            try {
              await auth().createUserWithEmailAndPassword(email, password);
            } catch (e) {
              var errorCode = e.code;
              switch (errorCode) {
                case 'auth/email-already-in-use':
                  Alert.alert('This already account with given email address.');
                  break;
                case 'auth/invalid-email':
                  Alert.alert('Invalid email address.');
                  break;
                case 'auth/operation-not-allowed':
                  Alert.alert('Email address or password is not available.');
                  break;
                case 'auth/weak-password':
                  Alert.alert('Password is not strong enough.');
                  break;
                default:
                  Alert.alert('Unknown error occured.');
              }
            }
          } else {
            Alert.alert('Fill email and password fields.');
          }

        },
        logout: async () => {
          if (googleCred) {
            try {
              await GoogleSignin.revokeAccess();
              await GoogleSignin.signOut();
            } catch (e) {
              console.error(e);
              Alert.alert('Something went wrong during logout!', e.message);
            }

          }
          try {
            await auth().signOut();
          } catch (e) {
            console.error(e);
            Alert.alert('Something went wrong during logout!', e.message);
          }
        }
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};