import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from '../screens/HomeScreen';
import BorrowScreen from '../screens/BorrowScreen';
import LendScreen from '../screens/LendScreen';
import BorrowedScreen from '../screens/BorrowedScreen';
import LendedScreen from '../screens/LendedScreen';
const Stack = createStackNavigator();
export default function HomeStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen name='Home' component={HomeScreen} />
      <Stack.Screen name='Borrow' component={BorrowScreen} />
      <Stack.Screen name='Lend' component={LendScreen} />
      <Stack.Screen name='Borrowed' component={BorrowedScreen} />
      <Stack.Screen name='Lended' component={LendedScreen} />
    </Stack.Navigator>
  );
}