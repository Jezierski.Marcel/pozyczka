import React from 'react';
import { StyleSheet, TextInput, View, Text } from 'react-native';
import { Icon } from 'react-native-elements';
import { windowHeight, windowWidth } from '../utils/Dimensions';
export default class FormDateInput extends React.Component {

    render() {
        return (
            <View style={styles.viewStyle}>
                <Text style={{ color: '#666', fontSize: 14, fontFamily: 'sans-serif' }}>
                     Return date:
                     <Text style={{ fontSize: 14, color: '#000', fontFamily: 'sans-serif' }}> {this.props.text} </Text>
                 </Text>
                <Icon
                    style={styles.iconStyle}
                    name='calendar'
                    type='font-awesome'
                    color='#f50' />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    viewStyle: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        padding: 5,
        marginTop: 5,
        marginBottom: 10,
        width: windowWidth / 1.5,
        height: windowHeight / 15,

        borderRadius: 8,
        borderWidth: 1
    },
    input: {
        fontSize: 16,
    },
    iconStyle: {
        alignItems: "center",
        marginHorizontal: 10,
        paddingHorizontal: 10,
    }
});