import React from 'react';
import { View, ActivityIndicator, StyleSheet, ImageBackground } from 'react-native';
export default function Loading() {
  return (
    <ImageBackground source={require('../utils/pawel-czerwinski-I5A7oW933yE-unsplash.jpg')}
      style={styles.loadingContainer}
      imageStyle={{
        opacity: 0.666,
      }}>
      <ActivityIndicator size='large' color='#6646ee' />
    </ImageBackground>
  );
}
const styles = StyleSheet.create({
  loadingContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  }
});