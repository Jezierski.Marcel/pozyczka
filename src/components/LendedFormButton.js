import React from 'react';
import { StyleSheet, TouchableOpacity, Text } from 'react-native';
import { windowHeight, windowWidth } from '../utils/Dimensions';
import { Icon } from 'react-native-elements';

export default function FormButton({ buttonTitle, ...rest }) {
    return (
        <TouchableOpacity style={styles.buttonContainer} {...rest}>
            <Text style={styles.buttonText}>{buttonTitle}</Text>
            <Icon
                style={styles.iconStyle}
                name='list'
                type='font-awesome'
                color='#cc35b5'/>
        </TouchableOpacity>

    );
}

const styles = StyleSheet.create({
    buttonContainer: {
        flexDirection: 'row',
        justifyContent: "space-between",
        marginTop: 10,
        width: windowWidth / 2,
        height: windowHeight / 15,
        backgroundColor: '#219666',
        padding: 10,
        alignItems: 'center',
        borderRadius: 8
    },
    buttonText: {
        fontSize: 28,
        color: '#ffffff',
        fontFamily: 'sans-serif',
    },
    iconStyle: {
        alignItems: "center",
        marginHorizontal: 10,
        paddingHorizontal: 10,
    }
});