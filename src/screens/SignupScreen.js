import React, { useState, useContext  } from 'react';
import { View, Text, StyleSheet, ImageBackground } from 'react-native';
import FormButton from '../components/FormButton';
import FormInput from '../components/FormInput';
import {updateProfile} from '../db/apiService';
import { AuthContext } from '../navigation/AuthProvider';

export default function SignupScreen({navigation}) {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const { register } = useContext(AuthContext);
  const { user } = useContext(AuthContext);

  return (
    <ImageBackground source={require('../utils/pawel-czerwinski-I5A7oW933yE-unsplash.jpg')}
    style={styles.container}
    imageStyle={{
      opacity: 0.666,
    }}>
      <Text style={styles.text}>Create an account</Text>
      <View style={{flex: 0.2}} />
      <FormInput
        value={email}
        placeholderText='Email'
        onChangeText={userEmail => setEmail(userEmail)}
        autoCapitalize='none'
        keyboardType='email-address'
        autoCorrect={false}
      />
      <View style={{flex: 0.05}} />
      <FormInput
        value={password}
        placeholderText='Password'
        onChangeText={userPassword => setPassword(userPassword)}
        secureTextEntry={true}
      />
      <View style={{flex: 0.1}} />
      <FormButton
        buttonTitle='Signup'
        onPress={() => register(email, password)}
      />
    </ImageBackground>
  );
}
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#c0dfed',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  space: {
    flex: 0.1
  },
  text: {
    fontSize: 36,
    marginBottom: 10,
    fontFamily: 'sans-serif',
  }
});