import React, { useContext, useState, useEffect, } from 'react';
import { View, Text, StyleSheet, FlatList, Button, TouchableHighlight, Modal, Alert, ImageBackground } from 'react-native';
import Swiper from 'react-native-web-swiper';
import database from '@react-native-firebase/database';
import { AuthContext } from '../navigation/AuthProvider';
import { returnItem } from '../db/apiService';
import FormButton from '../components/FormButton';
import Loading from '../components/Loading';


export default function BorrowedScreen({ navigation }) {

    const { user } = useContext(AuthContext);
    const [borrowedItems, setBorrowedItems] = useState([]);
    const [dataLoaded, setDataLoaded] = useState(false);
    const [confirmModal, setCofirmModal] = useState(false);
    const [succesModal, setSuccesModal] = useState(false);
    const [returningItem, setReturningItem] = useState(new Object());

    const getData = () => {
        setDataLoaded(false);
        database()
            .ref('/' + user.uid + '/items/borrowed')
            .once('value', (snapshot) => {
                const userItems = snapshot.val();
                let items = userItems ? Object.values(userItems) : null;
                setBorrowedItems(items);
                setDataLoaded(!dataLoaded)
            });
    }


    useEffect(() => {
        getData();
    }, []);


    const itemList = () => {
        let arr = borrowedItems;
        return arr.map((item) => {
            return (
                <View key={item.Id} style={styles.container}>
                    <ImageBackground source={require('../utils/pawel-czerwinski-I5A7oW933yE-unsplash.jpg')}
                        style={[styles.slideContainer,
                        (new Date() >= new Date(item.ReturnDate)) ? styles.warnSlide : styles.slide]}
                        imageStyle={{
                            opacity: 0.666,
                        }}>

                        <Text>
                            <Text style={styles.slideText}> Item: </Text>
                            <Text style={styles.itemText}> {item.Name} </Text>
                        </Text>
                        <View style={{ flex: 0.1 }}></View>
                        <Text>
                            <Text style={styles.slideText}> Lender :</Text>
                            <Text style={styles.itemText}> {item.Lender} </Text>
                        </Text>
                        <View style={{ flex: 0.1 }}></View>
                        <Text>
                            <Text style={styles.slideText}> Borrow date: </Text>
                            <Text style={styles.itemText}> {item.BorrowDate} </Text>
                        </Text>
                        <View style={{ flex: 0.1 }}></View>
                        <Text>
                            <Text style={styles.slideText}> Return date: </Text>
                            <Text style={styles.itemText}> {item.ReturnDate} </Text>
                        </Text>
                        <View style={{ flex: 0.2 }}></View>
                        <FormButton buttonTitle='RETURN ITEM' onPress={() => {
                            setCofirmModal(!confirmModal);
                            let itemCopy = Object.assign({}, item);
                            setReturningItem(itemCopy);
                        }
                        }
                        />
                    </ImageBackground>
                </View>
            );
        });
    };


    return (
        <View style={styles.container}>
            {((dataLoaded) ?
                ((borrowedItems) ?
                    <Swiper>
                        {itemList()}
                    </Swiper>
                    :
                    <ImageBackground source={require('../utils/pawel-czerwinski-I5A7oW933yE-unsplash.jpg')}
                        style={[styles.container, { flex: 1, alignItems: 'center', justifyContent: 'center', }]}
                        imageStyle={{
                            opacity: 0.666,
                        }}>
                        <Text style={{
                            textAlign: 'center',
                            color: 'purple',
                            fontSize: 50,
                            fontFamily: 'sans-serif',
                            fontWeight: 'bold'
                        }}>
                            You have no borrowed items in your list {'\u2728'}
                        </Text>
                    </ImageBackground>
                )
                :
                <Loading></Loading>
            )}

            <Modal
                animationType="fade"
                transparent={true}
                visible={confirmModal}
                onRequestClose={() => {
                    Alert.alert("Modal has been closed.");
                }}
            >
                <View style={modalStyles.centeredView}>
                    <View style={modalStyles.modalView}>
                        <Text style={modalStyles.modalText}> DO YOU CONFIRM? </Text>
                        <View style={{ flex: 0.2 }}></View>
                        <View style={{ flexDirection: 'row' }}>
                            <TouchableHighlight
                                style={{ ...modalStyles.openButton}}
                                onPress={() => {
                                    setCofirmModal(!confirmModal);
                                }}
                                underlayColor={"pink"}
                            >
                                <Text style={modalStyles.textStyle}>NO</Text>
                            </TouchableHighlight>
                            <View style={{ flex: 0.2 }}></View>
                            <TouchableHighlight
                                style={{ ...modalStyles.openButton}}
                                onPress={() => {
                                    setCofirmModal(!confirmModal);
                                    console.log(returningItem.Id);
                                    returnItem(returningItem.Id, returningItem.Name, returningItem.Lender, returningItem.Borrower, returningItem.LendDate, returningItem.ReturnDate);
                                    setSuccesModal(!succesModal);
                                    //navigation.goBack();
                                }}
                                underlayColor={"pink"}
                            >
                                <Text style={modalStyles.textStyle}>YES</Text>
                            </TouchableHighlight>
                        </View>
                    </View>
                </View>
            </Modal>

            <Modal
                animationType="fade"
                transparent={true}
                visible={succesModal}
                onRequestClose={() => {
                    Alert.alert("Modal has been closed.");
                }}
            >
                <View style={modalStyles.centeredView}>
                    <View style={modalStyles.modalView}>

                        <View style={{ flex: 0.2 }}></View>
                        <TouchableHighlight
                            style={{ ...modalStyles.openButton }}
                            onPress={() => {
                                setSuccesModal(!succesModal);
                                navigation.goBack();
                            }}
                            underlayColor={"pink"}
                        >
                            <Text style={modalStyles.modalText}> Return succeded! </Text>
                        </TouchableHighlight>

                    </View>
                </View>
            </Modal>

        </View >

    );
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    slideContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    slideText: {
        fontSize: 24,
        fontWeight: "bold",
        textAlign: "center",
        fontFamily: 'sans-serif'
    },
    itemText: {
        fontSize: 24,
        textAlign: "center",
        fontFamily: 'sans-serif'
    },
    warnSlide: {
        borderColor: "red",
        borderWidth: 6,
    },
    slide: {
        borderWidth: 6,
    },
});

const modalStyles = StyleSheet.create({
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22
    },
    modalView: {
        flex: 0.15,
        margin: 20,
        backgroundColor: "#c0dfed",
        borderRadius: 20,
        padding: 25,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    openButton: {
        backgroundColor: "#0088ff",
        borderRadius: 20,
        padding: 20,
        elevation: 2
    },
    textStyle: {
        color: 'black',
        fontWeight: "bold",
        textAlign: "center",
        fontFamily: 'sans-serif'
    },
    buttonTextStyle: {
        color: 'black',
        fontWeight: "bold",
        textAlign: "center",
        fontFamily: 'sans-serif'
    },
    modalText: {
        fontSize: 20,
        color: 'black',
        fontFamily: 'sans-serif',
        marginBottom: 15,
        textAlign: "center"
    },
});
