import React, { useContext, useState } from 'react';
import { View, Text, StyleSheet, ImageBackground, Modal, Alert, TouchableHighlight } from 'react-native';
import FormButton from '../components/FormButton';
import BorrowFormButton from '../components/BorrowFormButton';
import LendFormButton from '../components/LendFormButton';
import BorrowedFormButton from '../components/BorrowedFormButton';
import LendedFormButton from '../components/LendedFormButton';
import { AuthContext } from '../navigation/AuthProvider';
import { Icon } from 'react-native-elements';
export default function HomeScreen({ navigation }) {
  const { user, logout } = useContext(AuthContext);
  const [infoModal, setInfoModal] = useState(false);

  return (
    <ImageBackground source={require('../utils/pawel-czerwinski-I5A7oW933yE-unsplash.jpg')}
      style={styles.container}
      imageStyle={{
        opacity: 0.666,
      }}>
      <View style={{ flex: 0.1, flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'flex-end', marginHorizontal: 20 }}>
        <Icon
          onPress={() => setInfoModal(!infoModal)}
          style={styles.iconStyle}
          name='info-circle'
          type='font-awesome'
          color='#6646ee'
          size={38}
        />
      </View>
      <View style={styles.viewContainer}>
        <Text style={styles.welcomeText}>
          Welcome to
      </Text>
        <Text style={styles.welcomeText}>
          POŻYCZKA
      </Text>
        <View style={{ flex: 0.1 }} />
        {((user.displayName) ?
          <Text style={styles.userText}>
            {user.displayName}
          </Text>
          :
          <Text style={styles.userText}>
            {user.email}
          </Text>
        )}

        <View style={{ flex: 0.1 }} />
        <Text style={styles.text}>Add new item to my lists</Text>
        <BorrowFormButton buttonTitle='Borrow' onPress={() => navigation.navigate('Borrow')} />
        <LendFormButton buttonTitle='Lend' onPress={() => navigation.navigate('Lend')} />
        <View style={{ flex: 0.1 }} />
        <Text style={styles.text}>My lists</Text>
        <BorrowedFormButton buttonTitle='Borrowed' onPress={() => navigation.navigate('Borrowed')} />
        <LendedFormButton buttonTitle='Lended' onPress={() => navigation.navigate('Lended')} />
        <View style={{ flex: 0.4 }} />
        <FormButton buttonTitle='Logout' onPress={() => logout()} />
      </View>

      <Modal
        animationType="fade"
        transparent={true}
        visible={infoModal}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
        }}
      >
        <View style={modalStyles.centeredView}>
          <View style={modalStyles.modalView}>
            <Text style={[modalStyles.textStyle, { fontWeight: 'bold', fontStyle: 'italic' }]}>User information:</Text>
            <Text style={modalStyles.textStyle}>User name:</Text>
            <Text style={[modalStyles.textStyle, { fontWeight: 'bold', fontSize: 20 }]} selectable>{user.displayName}</Text>
            <Text style={modalStyles.textStyle}>User id:</Text>
            <Text style={[modalStyles.textStyle, { fontWeight: 'bold', fontSize: 20 }]} selectable>{user.uid}</Text>
            {/* <Text style={[modalStyles.textStyle, { fontSize: 15, marginVertical: 10 }]}>
              Use your friend's ID instead of his name
              to automatical synchronization
              between yours borrowed and lended items.
            </Text> */}
            <TouchableHighlight
              style={{ ...modalStyles.openButton, backgroundColor: "#c5e82a" }}
              onPress={() => {
                setInfoModal(!infoModal);
              }}
            >
              <Text style={modalStyles.modalText}>OK</Text>
            </TouchableHighlight>
          </View>
        </View>
      </Modal>



    </ImageBackground>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  viewContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    opacity: 1, //'#f5f5f1'
  },
  leftStyle: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  welcomeText: {
    justifyContent: 'center',
    fontSize: 32,
    fontFamily: 'sans-serif-condensed',
    color: '#333333',
    alignContent: 'flex-start',
    fontWeight: 'bold'
  },
  userText: {
    justifyContent: 'center',
    fontSize: 28,
    fontFamily: 'sans-serif',
    color: '#333333',
    alignContent: 'flex-start',
    fontStyle: 'italic'
  },
  text: {
    justifyContent: 'center',
    fontSize: 20,
    color: '#333333',
    alignContent: 'flex-start',
    fontFamily: 'sans-serif'
  },
  iconStyle: {
    marginHorizontal: 10,
    paddingHorizontal: 10,

    opacity: 0.8
  }
});

const modalStyles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22
  },
  modalView: {
    margin: 20,
    backgroundColor: "#6646ee",
    borderRadius: 20,
    opacity: 0.9,
    padding: 15,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  openButton: {
    backgroundColor: "#F194FF",
    borderRadius: 20,
    padding: 10,
    elevation: 2
  },
  textStyle: {
    color: "white",
    textAlign: "center",
    fontFamily: 'sans-serif',
    fontSize: 18,
  },
  modalText: {
    color: "white",
    fontWeight: "bold",
    marginHorizontal: 15,
    textAlign: "center",
    fontFamily: 'sans-serif',
    fontSize: 16
  }
});