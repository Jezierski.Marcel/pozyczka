import React, { useState, useContext} from 'react';
import { View, Text, StyleSheet, TouchableOpacity, ImageBackground } from 'react-native';
import FormButton from '../components/FormButton';
import FormInput from '../components/FormInput';
import { AuthContext } from '../navigation/AuthProvider';

export default function LoginScreen({ navigation }) {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const { login } = useContext(AuthContext);
    const { googleLogin } = useContext(AuthContext);
    return (
      <ImageBackground source={require('../utils/pawel-czerwinski-I5A7oW933yE-unsplash.jpg')}
      style={styles.container}
      imageStyle={{
        opacity: 0.666,
      }}> 
        <Text style={styles.welcomeText}>Pożyczka</Text>
        <View style={{flex: 0.2}} />
        <FormInput
          value={email}
          placeholderText='Email'
          onChangeText={userEmail => setEmail(userEmail)}
          autoCapitalize='none'
          keyboardType='email-address'
          autoCorrect={false}
        />
        <View style={{flex: 0.05}} />
        <FormInput
          value={password}
          placeholderText='Password'
          onChangeText={userPassword => setPassword(userPassword)}
          secureTextEntry={true}
        />
        <View style={{flex: 0.05}} />
        <FormButton buttonTitle='Login' onPress={() => login(email, password)} />
        <FormButton buttonTitle='Google Login' onPress={() => googleLogin()} />
        <TouchableOpacity
          style={styles.navButton}
          onPress={() => navigation.navigate('Signup')}
        >
          <Text style={styles.navButtonText}>New user? Join here</Text>
        </TouchableOpacity>
        </ImageBackground>
    );
  }

  const styles = StyleSheet.create({
    container: {
      backgroundColor: '#c0dfed',
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center'
    },
    text: {
      fontSize: 24,
      marginBottom: 10,
      fontFamily: 'sans-serif',
    },
    welcomeText: {
      justifyContent: 'center',
      fontSize: 40,
      fontFamily: 'sans-serif-condensed',
      color: '#333333',
      alignContent: 'flex-start',
      fontWeight: 'bold'
    },
    navButton: {
      marginTop: 15
    },
    navButtonText: {
      fontSize: 20,
      color: '#6646ee'
    }
  });