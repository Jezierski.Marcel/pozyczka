import React, { useState, useContext } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Modal, Alert, TouchableHighlight, ImageBackground } from 'react-native';
import FormButton from '../components/FormButton';
import BorrowFormButton from '../components/BorrowFormButton';
import FormInput from '../components/FormInput';
import { windowHeight, windowWidth } from '../utils/Dimensions';
import database from '@react-native-firebase/database';
import { borrowItem } from '../db/apiService';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import { ThemeProvider } from 'react-native-elements';
import FormDateInput from '../components/FormDateInput';
import { AuthContext } from '../navigation/AuthProvider';

export default function BorrowScreen({navigation}) {
  const { user } = useContext(AuthContext);
  const [item, setItem] = useState('');
  const [borrower, setBorrower] = useState('');
  const [lender, setLender] = useState('');
  const [returnDate, setReturnDate] = useState();
  const [dateTimePickerVisible, setDateTimePickerVisible] = useState(false);
  const [errModalVisible, setErrModalVisible] = useState(false);
  const [successModalVisible, setSuccessModalVisible] = useState(false);

  const showCalendar = () => {
    setDateTimePickerVisible(true);
  }
  const hideCalendar = () => {
    setDateTimePickerVisible(false);
  }
  const handleConfirm = (date) => {
    setReturnDate(date);
    hideCalendar();
  }

  return (
    <ImageBackground 
    source={require('../utils/pawel-czerwinski-I5A7oW933yE-unsplash.jpg')}
    style={styles.container}
    imageStyle={{opacity: 0.666}}>
      <Text style={styles.borrowHeader}>Borrow an item</Text>
      <FormInput
        value={item}
        placeholderText='Item'
        onChangeText={borrowedItem => setItem(borrowedItem)}
        autoCapitalize='none'
        autoCorrect={false}
      />

      <FormInput
        value={lender}
        placeholderText='Lender'
        onChangeText={lender => setLender(lender)}
        autoCapitalize='none'
        autoCorrect={false}
      />

      <DateTimePickerModal
        isVisible={dateTimePickerVisible}
        value={returnDate}
        mode='date'
        display='calendar'
        onConfirm={handleConfirm}
        onCancel={hideCalendar}
        minimumDate={new Date()}
      />

      <TouchableOpacity onPress={showCalendar}>
        <FormDateInput
          text={returnDate ? returnDate.toDateString() : ''}
        />
      </TouchableOpacity>

      <BorrowFormButton buttonTitle='Borrow' onPress={
        () => {(item && lender && returnDate) ? 
          borrowItem(null, item, lender, user.uid, new Date().toDateString(), returnDate.toDateString()).then(setSuccessModalVisible(!successModalVisible)) :
          setErrModalVisible(!errModalVisible)}
        } 
      />

        <Modal
          animationType="fade"
          transparent={true}
          visible={errModalVisible}
          onRequestClose={() => {
            Alert.alert("Modal has been closed.");
          }}
        >
          <View style={modalStyles.centeredView}>
            <View style={{ ...modalStyles.modalView, backgroundColor: 'red'}}>
              <TouchableHighlight
                style={{ ...modalStyles.openButton, color: 'red'}}
                onPress={() => {
                  setErrModalVisible(!errModalVisible);
                }}
                underlayColor = {'red'}
              >
                <Text style={modalStyles.textStyle}>You have not{'\n'}filled all fields!</Text>
              </TouchableHighlight>
            </View>
          </View>
        </Modal>


        <Modal
          animationType="fade"
          transparent={true}
          visible={successModalVisible}
          onRequestClose={() => {
            Alert.alert("Modal has been closed.");
          }}
        >
          <View style={modalStyles.centeredView}>
            <View style={{ ...modalStyles.modalView, backgroundColor:'green'}}>
              <TouchableHighlight
                style={{ ...modalStyles.openButton, color: 'green'}}
                onPress={() => {
                  setSuccessModalVisible(!successModalVisible);
                  navigation.goBack();
                }}
                underlayColor = {'green'}
              >
                <Text style={modalStyles.textStyle}>Success!{'\n'} You have borrowed:  {item} {'\n'} From: {lender}</Text>
              </TouchableHighlight>
            </View>
          </View>
        </Modal>


    </ImageBackground>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#c0dfed'//'#f5f5f1'
  },
  borrowHeader: {
    fontFamily: 'sans-serif-medium',
    fontWeight: 'bold',
    fontSize: 38,
    marginBottom: 50
  },
  text: {
    fontSize: 20,
    color: '#333333',
    alignContent: 'flex-start'
  },
  input: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    padding: 10,
    marginTop: 5,
    marginBottom: 10,
    width: windowWidth / 1.5,
    height: windowHeight / 15,
    fontSize: 16,
    borderRadius: 8,
    borderWidth: 1
  }
});


const modalStyles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 10,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  openButton: {
    backgroundColor: "#7ff0bb",
    borderRadius: 20,
    padding: 10,
    elevation: 2
  },
  textStyle: {
    color: 'black',
    fontWeight: "bold",
    textAlign: "center",
    fontFamily: 'sans-serif',
    fontSize: 20
  }
});