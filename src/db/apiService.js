import database from '@react-native-firebase/database';
export const borrowItem = (Id, Name, Lender, Borrower, BorrowDate, ReturnDate) => {
  return new Promise(function (resolve, reject) {
    let key;
    if (Id != null) {
      key = Id;
    } else {
      key = database()
        .ref('/' + Borrower + '/items/borrowed')
        .push().key;
    }
    let dataToSave = {
      Id: key,
      Name: Name,
      Lender: Lender,
      Borrower: Borrower,
      BorrowDate: BorrowDate,
      ReturnDate: ReturnDate,
    };
    console.log(dataToSave);
    database()
      .ref('/' + Borrower + '/items/borrowed/' + key)
      .update(dataToSave)
      .then(snapshot => {
        resolve(snapshot);
        if (Id == null) {
          lendItem(key, Name, Lender, Borrower, BorrowDate, ReturnDate);
        }
      })
      .catch(err => {
        reject(err);
      });
  });
};



export const lendItem = (Id, Name, Lender, Borrower, LendDate, ReturnDate) => {
  return new Promise(function (resolve, reject) {
    let key;
    if (Id != null) {
      key = Id;
    } else {
      key = database()
        .ref('/' + Lender + '/items/lended/')
        .push().key;
    }
    let dataToSave = {
      Id: key,
      Name: Name,
      Lender: Lender,
      Borrower: Borrower,
      BorrowDate: LendDate,
      ReturnDate: ReturnDate
    };
    console.log(dataToSave);
    database()
      .ref('/' + Lender + '/items/lended/' + key)
      .update(dataToSave)
      .then(snapshot => {
        resolve(snapshot);
        if (Id == null) {
          borrowItem(key, Name, Lender, Borrower, LendDate, ReturnDate);
        }
      })
      .catch(err => {
        reject(err);
      });
  });
};

export const updateProfile = (userId, name) => {
  console.log('UID', userId);
  console.log('DISPLAY NAME', displayName);
  user.updateProfile({
    displayName: displayName
  });
  database()
    .ref('/' + userId + '/' + displayName)
    .update(dataToSave)
    .then(snapshot => {
      resolve(snapshot);
    })
    .catch(err => {
      reject(err);
    });
};




export const returnItem = (Id, Name, Lender, Borrower, LendDate, ReturnDate) => {
  database().ref('/' + Lender + '/items/lended/' + Id).remove();
  database().ref('/' + Borrower + '/items/borrowed/' + Id).remove();
};