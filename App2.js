// /**
//  * Sample React Native App
//  * https://github.com/facebook/react-native
//  *
//  * @format
//  * @flow strict-local
//  */

// import React from 'react';
// import {
//   SafeAreaView,
//   StyleSheet,
//   ScrollView,
//   View,
//   Text,
//   StatusBar,
// } from 'react-native';

// import {
//   Header,
//   LearnMoreLinks,
//   Colors,
//   DebugInstructions,
//   ReloadInstructions,
// } from 'react-native/Libraries/NewAppScreen';

// const App: () => React$Node = () => {
//   return (
//     <>
//       <StatusBar barStyle="dark-content" />
//       <SafeAreaView>
//         <ScrollView
//           contentInsetAdjustmentBehavior="automatic"
//           style={styles.scrollView}>
//           <Header />
//           {global.HermesInternal == null ? null : (
//             <View style={styles.engine}>
//               <Text style={styles.footer}>Engine: Hermes</Text>
//             </View>
//           )}
//           <View style={styles.body}>
//             <View style={styles.sectionContainer}>
//               <Text style={styles.sectionTitle}>Step One</Text>
//               <Text style={styles.sectionDescription}>
//                 Edit <Text style={styles.highlight}>App.js</Text> to change this
//                 screen and then come back to see your edits.
//               </Text>
//             </View>
//             <View style={styles.sectionContainer}>
//               <Text style={styles.sectionTitle}>See Your Changes</Text>
//               <Text style={styles.sectionDescription}>
//                 <ReloadInstructions />
//               </Text>
//             </View>
//             <View style={styles.sectionContainer}>
//               <Text style={styles.sectionTitle}>Debug</Text>
//               <Text style={styles.sectionDescription}>
//                 <DebugInstructions />
//               </Text>
//             </View>
//             <View style={styles.sectionContainer}>
//               <Text style={styles.sectionTitle}>Learn More</Text>
//               <Text style={styles.sectionDescription}>
//                 Read the docs to discover what to do next:
//               </Text>
//             </View>
//             <LearnMoreLinks />
//           </View>
//         </ScrollView>
//       </SafeAreaView>
//     </>
//   );
// };

// const styles = StyleSheet.create({
//   scrollView: {
//     backgroundColor: Colors.lighter,
//   },
//   engine: {
//     position: 'absolute',
//     right: 0,
//   },
//   body: {
//     backgroundColor: Colors.white,
//   },
//   sectionContainer: {
//     marginTop: 32,
//     paddingHorizontal: 24,
//   },
//   sectionTitle: {
//     fontSize: 24,
//     fontWeight: '600',
//     color: Colors.black,
//   },
//   sectionDescription: {
//     marginTop: 8,
//     fontSize: 18,
//     fontWeight: '400',
//     color: Colors.dark,
//   },
//   highlight: {
//     fontWeight: '700',
//   },
//   footer: {
//     color: Colors.dark,
//     fontSize: 12,
//     fontWeight: '600',
//     padding: 4,
//     paddingRight: 12,
//     textAlign: 'right',
//   },
// });

// export default App;








// export interface Props {
//   name: string;
//   enthusiasmLevel?: number;
// }

// const Hello: React.FC<Props> = (props) => {
//   const [enthusiasmLevel, setEnthusiasmLevel] = React.useState(
//     props.enthusiasmLevel
//   );

//   const onIncrement = () =>
//     setEnthusiasmLevel((enthusiasmLevel || 0) + 1);
//   const onDecrement = () =>
//     setEnthusiasmLevel((enthusiasmLevel || 0) - 1);

//   const getExclamationMarks = (numChars: number) =>
//     Array(numChars + 1).join('!');
//   return (
//     <View style={styles.root}>
//       <Text style={styles.greeting}>
//         Hello{' '}
//         {props.name + getExclamationMarks(enthusiasmLevel || 0)}
//       </Text>
//       <View style={styles.buttons}>
//         <View style={styles.button}>
//           <Button
//             title="-"
//             onPress={onDecrement}
//             accessibilityLabel="decrement"
//             color="red"
//           />
//         </View>
//         <View style={styles.button}>
//           <Button
//             title="+"
//             onPress={onIncrement}
//             accessibilityLabel="increment"
//             color="blue"
//           />
//         </View>
//       </View>
//     </View>
//   );
// };

// const styles = StyleSheet.create({
//   root: {
//     alignItems: 'center',
//     alignSelf: 'center'
//   },
//   buttons: {
//     flexDirection: 'row',
//     minHeight: 70,
//     alignItems: 'stretch',
//     alignSelf: 'center',
//     borderWidth: 5
//   },
//   button: {
//     flex: 1,
//     paddingVertical: 0
//   },
//   greeting: {
//     color: '#999',
//     fontWeight: 'bold'
//   }
// });

// export default Hello;



import React, { useState, useEffect, Component } from 'react';
import { Button, StyleSheet, Text, View, TextInput, TouchableOpacity } from 'react-native';
import auth from '@react-native-firebase/auth';
import { GoogleSignin } from '@react-native-community/google-signin';

GoogleSignin.configure({
  webClientId: "853964595557-98ksm0jj7dmknq8ephi7ps4oldm7c10v.apps.googleusercontent.com",
});
//853964595557-98ksm0jj7dmknq8ephi7ps4oldm7c10v.apps.googleusercontent.com
//853964595557-p8u1ctqt305n35k2ctdf8eobdp54m2b0.apps.googleusercontent.com



function LoginApp() {
  // Set an initializing state whilst Firebase connects
  const [initializing, setInitializing] = useState(true);
  const [user, setUser] = useState();

  // Handle user state changes
  function onAuthStateChanged(user) {
    setUser(user);
    if (initializing) setInitializing(false);
  }

  useEffect(() => {
    const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
    return subscriber; // unsubscribe on unmount
  }, []);

  if (initializing) return null;

  if (!user) {
    return (
      <View>
        <Text>Login</Text>
      </View>
    );
  }

  return (
    <View>
      <Text>Welcome {user.email}</Text>
    </View>
  );
}



// const App = () => (

//   <View style={styles.root}>
//        <Text style={styles.greeting}>
//          POZYCZKA
//        </Text>
//     <LoginApp></LoginApp>
//   </View>
// );



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#003f5c',
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo:{
    fontWeight: 'bold',
    fontSize: 50,
    color: "#fb5b5a",
    marginBottom : 40
  },
  inputView:{
    width:"80%",
    backgroundColor:"#465881",
    borderRadius:25,
    height:50,
    marginBottom:20,
    justifyContent:"center",
    padding:20
  },
  inputText:{
    height:50,
    color:"white"
  },
  forgot:{
    color:"white",
    fontSize:11,
  },
  loginText:{
    color:"white",
    fontSize:16
  },
  loginBtn:{
    width:"80%",
    backgroundColor:"#fb5b5a",
    borderRadius:25,
    height:50,
    alignItems:"center",
    justifyContent:"center",
    marginTop:40,
    marginBottom:10
  },
  root: {
    alignItems: 'center',
    alignSelf: 'center'
  },
  buttons: {
    flexDirection: 'row',
    minHeight: 70,
    alignItems: 'stretch',
    alignSelf: 'center',
    borderWidth: 5
  },
  button: {
    flex: 1,
    paddingVertical: 0
  },
  greeting: {
    color: '#999',
    fontWeight: 'bold'
  }
});



class App extends Component {

  state={
    email: "",
    password: '',
  }

  createUser = () => {
    auth()
    .signInWithEmailAndPassword('jane.doe@example.com', 'SuperSecretPassword!')
    .then(() => {
      console.log('User account created & signed in!');
    })
    .catch(error => {
      if (error.code === 'auth/email-already-in-use') {
        console.log('That email address is already in use!');
      }

      if (error.code === 'auth/invalid-email') {
        console.log('That email address is invalid!');
      }

      console.error(error);
    });
  }

  logOff = () => {
    auth()
    .signOut()
    .then(() => console.log('User signed out!'));
  };

  googleLogOff = async () => {
    try{
      await GoogleSignin.revokeAccess();
      await GoogleSignin.signOut();
      auth()
      .signOut()
      .then(() => console.log('User signed out!'));
    } catch(error) {
      console.error(error);
    }
  };

  onGoogleButtonPress = async () => {
    // Get the users ID token
    const { idToken } = await GoogleSignin.signIn().then(console.log('GOOGLE SIGN IN')).catch(error => console.error(error));
    if(idToken==null){
      console.log('idToken is null or undefined');
    }else{
      console.log('ID TOKEN:');
      console.log(idToken);
    }
    // Create a Google credential with the token
    const googleCredential = auth.GoogleAuthProvider.credential(idToken);
    // Sign-in the user with the credential
    return auth().signInWithCredential(googleCredential);
  }


  componentDidMount(){
  }

  render() {
    const appName = 'Pozyczka';
    return (
      <View style={styles.container}>
        <Text style={styles.logo}>
          {appName}
        </Text>
        <View style={styles.inputView} >
          <TextInput  
            style={styles.inputText}
            placeholder="Email..." 
            placeholderTextColor="#003f5c"
            onChangeText={text => this.setState({email:text})}/>
        </View>
        <View style={styles.inputView} >
          <TextInput  
            style={styles.inputText}
            placeholder="Password..." 
            placeholderTextColor="#003f5c"
            onChangeText={text => this.setState({password:text})}
            secureTextEntry = {true}/>
        </View>
        <TouchableOpacity>
          <Text style={styles.forgot}>Forgot Password?</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.loginBtn}>
          <Text style={styles.loginText}>LOG IN</Text>
        </TouchableOpacity>
        <TouchableOpacity>
          <Text style={styles.loginText}>Sign up</Text>
        </TouchableOpacity>
        <Text style={{marginVertical: 5, color: 'white', fontSize: 12}}>OR</Text>
        <TouchableOpacity>
          <Text style={styles.loginText}>Sign in with Google</Text>
        </TouchableOpacity>
        <Button
          title="Google Sign-In"
          onPress={() => this.onGoogleButtonPress().then(() => console.log('Signed in with Google!'))}
        />
        <Button
          title="Google Sign-Out"
          onPress={() => this.googleLogOff().then(() => console.log('Signed out with Google!'))}
        />
        <LoginApp></LoginApp>
        <Button title="Log In As Jane" onPress={this.createUser}/>
        <Button title="Log Out" onPress={this.logOff}/>
      </View>
    );
  }
}

export default App;

// export default class App extends Component<Props> {
//   auth()
//   .signInAnonymously()
//   .then(() => {
//     console.log('User signed in anonymously');
//   })
//   .catch(error => {
//     if (error.code === 'auth/operation-not-allowed') {
//       console.log('Enable anonymous in your firebase console.');
//     }

//     console.error(error);
//   });
// };